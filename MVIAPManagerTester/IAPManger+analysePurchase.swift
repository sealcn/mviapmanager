//
//  IAPManger+analysePurchase.swift
//  MVIAPManagerTester
//
//  Created by rentonlin on 1/17/19.
//  Copyright © 2019 rentonlin. All rights reserved.
//

import Foundation
//
//  IAPManager+analysePurchase.swift
//  PeaceMeditation
//
//  Created by Renton on 2018/7/25.
//  Copyright © 2018 DAILYINNOVATION CO., LIMITED. All rights reserved.
//

import Foundation

extension IAPManager {
    func analysePurchase() {
        //TODO:  Renton iAP封装
        didFinishPurchaseBlock = { purchase in
            let productId = purchase.productId
            var type = "subscription"
            switch productId {
            case IAPProductID.lifeTime.rawValue:
                type = "nonconsumable"
            default: break
            }
            let iapProductID = IAPProductID(rawValue: productId)
            let price: Decimal? = iapProductID?.estimatedPrice()
            guard let decimalPrice = price else { return }
            print("purchase \(productId) \(decimalPrice) \(type)")
        }
        
        //TODO:  Renton iAP封装
        didRenewBlock = { purchase in
            let productId = purchase.productId
            var type = "subscription"
            switch productId {
            case IAPProductID.lifeTime.rawValue:
                type = "nonconsumable"
            default: break
            }
            let iapProductID = IAPProductID(rawValue: productId)
            let price: Decimal? = iapProductID?.estimatedPrice()
            guard let decimalPrice = price else { return }
            print("renew \(productId) \(decimalPrice) \(type)")
        }
    }
}
