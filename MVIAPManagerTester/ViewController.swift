//
//  ViewController.swift
//  MVIAPManagerTester
//
//  Created by rentonlin on 1/17/19.
//  Copyright © 2019 rentonlin. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {

    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        IAPManager.shared.initial()
        
        IAPManager.shared.subscribed.asObservable().subscribe(onNext: { (subscribed) in
            print(subscribed)
        }, onError: { (error) in
            print(error)
            }).disposed(by: disposeBag)
    }
    
    @IBAction func getProductInfo() {
        IAPManager.shared.getIAPs().subscribe(onNext: { (iaps) in
            print(iaps)
        }, onError: { (error) in
            print(error)
            }).disposed(by: disposeBag)
    }
    
    @IBAction func purchaseMonth() {
        IAPManager.shared.purchase(iAPId: IAPProductID.monthly.rawValue).subscribe(onNext: { (iaps) in
            print(iaps)
        }, onError: { (error) in
            print(error)
            }).disposed(by: disposeBag)
    }
    
    @IBAction func restore() {
        IAPManager.shared.restore().subscribe(onNext: { (status) in
            print(status)
        }, onError: { (error) in
            print(error)
            }).disposed(by: disposeBag)
    }
    
    @IBAction func purchaseLifetime() {
        IAPManager.shared.purchase(iAPId: IAPProductID.lifeTime.rawValue).subscribe(onNext: { id in
            print(id)
            }).disposed(by: disposeBag)
    }

    @IBAction func verifyAndGetUnexpiredPurchaseResult(_ sender: Any) {
        IAPManager.shared.verifyAndGetReceiptInfo().subscribe(onNext: { (items) in
            items.forEach { (item) in
                print(item)
            }
            }).disposed(by: disposeBag)
    }


}

