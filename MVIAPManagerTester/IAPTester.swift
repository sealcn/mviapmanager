//
//  IAPTester.swift
//  MVIAPManagerTester
//
//  Created by rentonlin on 1/17/19.
//  Copyright © 2019 rentonlin. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyStoreKit
import StoreKit
import MVIAPManager
fileprivate let kFarestExpireDateKey = "com.meevii.peace.iap.farest.expire.date"

//MARK: 模板
public enum IAPProductID: String, CaseIterable {
    case monthly = "com.meevii.peace.monthly"
    case monthlyWithFreeTrial = "com.meevii.peace.monthly.freetrial"
    case yearly = "com.meevii.peace.yearly"
    case yearlyWithFreeTrial = "com.meevii.peace.yearly.freetrial"
    case lifeTime = "com.meevii.peace.lifetime"
    case yearlyWithDiscount = "com.meevii.peace.yearly.discounted"
    case yearlySuperPremium = "com.meevii.peace.yearly.premium"
    
    static func getAllIAPAndType() -> [IAPProductID: IAPRxWrapper.IAPType] {
        let paris = IAPProductID.allCases.map { (iapProductId) -> (IAPProductID, IAPRxWrapper.IAPType) in
            return (iapProductId, iapProductId.iAPType)
        }
        let result = [IAPProductID: IAPRxWrapper.IAPType](uniqueKeysWithValues: paris)
        return result
    }
    
    //！！！！！！！这个方法别写default的值！！！！！！
    public func defaultPrice() -> String {
        switch self {
        case .monthly:
            return "$9.99"
        case .monthlyWithFreeTrial:
            return "$9.99"
        case .yearly:
            return "$59.99"
        case .yearlyWithFreeTrial:
            return "$59.99"
        case .lifeTime:
            return "$149.99"
        case .yearlyWithDiscount:
            return "$29.99"
        case .yearlySuperPremium:
            return "$64.99"
        }
    }
    
    private var iAPType: IAPRxWrapper.IAPType {
        get {
            switch self {
            case .lifeTime:
                return .Non_Consumable
            case .monthly, .monthlyWithFreeTrial, .yearly, .yearlyWithDiscount, .yearlyWithFreeTrial, .yearlySuperPremium:
                return .Auto_Renewable_Subscription
            }
        }
    }
    
    public func decimalPrice() -> Decimal {
        let priceString = defaultPrice()
        let price = (priceString as NSString).substring(from: 1)
        return Decimal(string: price) ?? Decimal(integerLiteral: 0)
    }
    
    public func estimatedPrice() -> Decimal {
        let originPrice = decimalPrice()
        switch self {
        case .monthlyWithFreeTrial, .yearlyWithFreeTrial:
            return originPrice * 0.3
        default:
            return originPrice
        }
    }
}

public class IAPPriceConfig {
    static let kDefaultMonthlyPriceOfYearly = "$4.99"//年订阅时一个月的价格
    static let kDefaultMonthlyPriceOfDiscountedYearly = "$2.49"//打折的年订阅一个月的价格
}

public class IAPCommonConfig {
    static let kDiscountValue = 67
}

private let kUserDefaultSubscribeKey = "com.peace.userDefault.subscribed"
private let kIAPSharedSecrect = "101648ba2e184e83bb8e7598fc4078f2"

typealias IAPMangerComleteBlock = (_ subscribed: Bool) -> Void

class IAPManager {
    static let shared = IAPManager()
    private let disposeBag = DisposeBag()
    
    init() {
        loadFromDisk()
        loadDefaultIAPs()
        updateShouldAddStorePaymentHander()
    }
    public func initial() {
        //必须在completePurchase之前调用anlysePurchase给对应的block的赋值，不然会错过renew的时机
        IAPManager.shared.analysePurchase()
        IAPManager.shared.completePurchase()
        IAPManager.shared.tryRestoreAccordingToLocalData()
        IAPManager.shared.updateIAPsFromServer()
    }
    
    private func loadFromDisk() {
        let v = UserDefaults.standard.bool(forKey: kUserDefaultSubscribeKey)
        subscribed.value = v
    }
    
    //MARK: SubscribeState
    let subscribed: Variable<Bool> = Variable<Bool>(false)
    private func setSubscribed(subscribed: Bool) {
        self.subscribed.value = subscribed
        UserDefaults.standard.set(subscribed, forKey: kUserDefaultSubscribeKey)
    }
    
    func getSubscribed() -> Observable<Bool> {
        return subscribed.asObservable().distinctUntilChanged()
    }
    
    private let cancelledSubscription = Variable<Bool?>(nil)
    func getCancelledSubscription() -> Observable<Bool> {
        //TODO:  Renton 这么写有点蠢
        return cancelledSubscription.asObservable().filter({ (cancelled) -> Bool in
            return cancelled != nil
        }).map({ (cancelled) in
            if let cancelled = cancelled, cancelled == true {
                return true
            }
            return false
        })
    }
    
    let expiredIds: Variable<[String]?> = Variable<[String]?>(nil)
    
    private let hasPaid = Variable<Bool?>(nil)
    func getHasPaid() -> Observable<Bool> {
        //TODO:  Renton 优化，这么写很蠢
        return hasPaid.asObservable().filter({ (paid) -> Bool in
            if let _ = paid {
                return true
            }
            return false
        }).map({ (paid) in
            if let paid = paid, paid == true {
                return true
            }
            return false
        }).distinctUntilChanged()
    }
    
    //MARK: IAP entities
    private let supportedIAPs: [IAPProductID: IAPRxWrapper.IAPType] = IAPProductID.getAllIAPAndType()
    
    private func loadDefaultIAPs() {
        let monthlyPlan = iAPItem(productId: IAPProductID.monthly, product: nil)
        let yearlyPlan = iAPItem(productId: IAPProductID.yearly, product: nil)
        let yearlyPlanWithFreeTrial = iAPItem(productId: IAPProductID.yearlyWithFreeTrial, product: nil)
        let lifeTimePlan = iAPItem(productId: IAPProductID.lifeTime, product: nil)
        let yearlyDiscountedPlan = iAPItem(productId: IAPProductID.yearlyWithDiscount, product: nil)
        let monthlyPlanWithFreeTrial = iAPItem(productId: IAPProductID.monthlyWithFreeTrial, product: nil)
        let yearlySuperPremiumPlan = iAPItem(productId: IAPProductID.yearlySuperPremium, product: nil)
        
        let sortedProducts: [iAPItem] = [monthlyPlan, yearlyPlan, yearlyPlanWithFreeTrial,
                                         lifeTimePlan,
                                         yearlyDiscountedPlan, monthlyPlanWithFreeTrial, yearlySuperPremiumPlan]
        iAPs.value = sortedProducts
    }
    
    func updateIAPsFromServer() {
        let productIds = Set(supportedIAPs.keys.map{$0.rawValue})
        MVIAPManager.shared.getSKProducts(with: productIds).subscribe(onNext: { [weak self] (products) in
            guard let s = self else { return }
            //products是无序的，所以要按顺序排一下
            //而且即使下架了也需要一个占位的东西
            var monthlyPlan = iAPItem(productId: IAPProductID.monthly, product: nil)
            var yearlyPlan = iAPItem(productId: IAPProductID.yearly, product: nil)
            var yearlyPlanWithFreeTrial = iAPItem(productId: IAPProductID.yearlyWithFreeTrial, product: nil)
            var lifeTimePlan = iAPItem(productId: IAPProductID.lifeTime, product: nil)
            var yearlyDiscountedPlan = iAPItem(productId: IAPProductID.yearlyWithDiscount, product: nil)
            var monthlyPlanWithFreetrial = iAPItem(productId: IAPProductID.monthlyWithFreeTrial, product: nil)
            var yearlySuperPremiumPlan = iAPItem(productId: IAPProductID.yearlySuperPremium, product: nil)
            
            //TODO:  Renton iAP封装
            for product in products {
                guard let iAPId = IAPProductID(rawValue: product.productIdentifier) else {
                    continue
                }
                let iAP = iAPItem(productId: iAPId, product: product)
                switch iAPId {
                case IAPProductID.monthly:
                    monthlyPlan = iAP
                case IAPProductID.monthlyWithFreeTrial:
                    monthlyPlanWithFreetrial = iAP
                case IAPProductID.yearly:
                    yearlyPlan = iAP
                case IAPProductID.yearlyWithFreeTrial:
                    yearlyPlanWithFreeTrial = iAP
                case IAPProductID.lifeTime:
                    lifeTimePlan = iAP
                case IAPProductID.yearlyWithDiscount:
                    yearlyDiscountedPlan = iAP
                case IAPProductID.yearlySuperPremium:
                    yearlySuperPremiumPlan = iAP
                }
            }
            let sortedProducts: [iAPItem] = [monthlyPlan, yearlyPlan, yearlyPlanWithFreeTrial,
                                             lifeTimePlan, yearlyDiscountedPlan, monthlyPlanWithFreetrial, yearlySuperPremiumPlan]
            s.iAPs.value = sortedProducts
            }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
    
    //MARK: Actions
    func restore() -> Observable<UserIAPStatus> {
        return restoreWithIAPs(supportedIAPAndType: supportedIAPs)
    }
    
    //MARK: Supported iAPS
    private let iAPs: Variable<[iAPItem]> = Variable<[iAPItem]>([])
    
    func getIAPs() -> Observable<[iAPItem]> {
        return iAPs.asObservable()
    }
    
    //MARK: Actions
    func purchase(iAP: iAPItem) -> Observable<String> {
        return purchase(iAPId: iAP.productId.rawValue)
    }
    
    func tryRestoreAccordingToLocalData() {
        if let _ = UserDefaults.standard.object(forKey: kFarestExpireDateKey) {
            if UserDefaults.standard.double(forKey: kFarestExpireDateKey) > Date().timeIntervalSince1970 {
                //有记录, 而且未过期,也不restore
                print("Local date not expired, cancel restore")
            } else {
                //有记录，但已过期,restore会刷新收据，如果续费了，就会更新日期，否则移除
                print("Local date expired, restoring")
                restore().subscribe().disposed(by: disposeBag)
            }
        } else {
            //没有记录，不restore
            print("No record, cancel restore, wait for user to restore")
        }
    }
    
    //MARK: 模板方法
    private func updateShouldAddStorePaymentHander() {
        MVIAPManager.shared.observePurchaseIAPFromAppStoreClosure = { [weak self] payment, product in
            DispatchQueue.main.async { [weak self] in
                print("try show freetrial")
            }
            return false
        }
    }
    
    //MARK: renew and finish block
    var didFinishPurchaseBlock: IAPManagerDidFinishPurchaseBlock? {
        get {
            return MVIAPManager.shared.didFinishPurchaseBlock
        }
        set {
            MVIAPManager.shared.didFinishPurchaseBlock = newValue
        }
    }
    var didRenewBlock: IAPManagerDidRenewBlock? {
        get {
            return MVIAPManager.shared.didRenewBlock
        }
        set {
            MVIAPManager.shared.didRenewBlock = newValue
        }
    }
    
    func verifyAndGetUnexpiredProductIds() -> Observable<[String]> {
        var mappedDictionary = [String: IAPRxWrapper.IAPType]()
        for (key, value) in supportedIAPs {
            mappedDictionary[key.rawValue] = value
        }
        return MVIAPManager.shared.verifyAndGetUnexpiredProductIds(mappedDictionary: mappedDictionary, secrect: kIAPSharedSecrect)
            .map { [weak self] ids in
                self?.setSubscribed(subscribed: ids.count > 0)
                return ids
        }
    }

    func verifyAndGetReceiptInfo() -> Observable<[ReceiptItem]> {
        var mappedDictionary = [String: IAPRxWrapper.IAPType]()
        for (key, value) in supportedIAPs {
            mappedDictionary[key.rawValue] = value
        }
        return MVIAPManager.shared.verifyAndGetUnexpiredPurchaseResult(mappedDictionary: mappedDictionary, secrect: kIAPSharedSecrect)
    }
    
    func completePurchase() {
        MVIAPManager.shared.completePurchase { [weak self] (hasPurchasedOrRestoredTranscation) in
            guard let strongSelf = self else { return }
            if hasPurchasedOrRestoredTranscation {
                strongSelf.verifyAndGetUnexpiredProductIds().subscribe().disposed(by: strongSelf.disposeBag)
            }
        }
    }
    
    func purchase(iAPId: String) -> Observable<String> {
        return MVIAPManager.shared.purchase(iAPId: iAPId).map{ [weak self] iAPId in
            guard let strongSelf = self else { return iAPId }
            strongSelf.setSubscribed(subscribed: true)
            //购买后，马上restore刷新当前的用户的最远过期时间
            strongSelf.restore().subscribe().disposed(by: strongSelf.disposeBag)
            return iAPId
        }
    }
    
    func restoreWithIAPs(supportedIAPAndType: [IAPProductID: IAPRxWrapper.IAPType]) -> Observable<UserIAPStatus> {
        var mappedDictionary = [String: IAPRxWrapper.IAPType]()
        for (key, value) in supportedIAPAndType {
            mappedDictionary[key.rawValue] = value
        }
        return MVIAPManager.shared.restoreWithIAPs(supportedIAPAndType: mappedDictionary, secret: kIAPSharedSecrect).map { iAPStatus in
            let activeProductIds = iAPStatus.activeProductIds
            let expiredProductIds = iAPStatus.expiredProductIds
            if activeProductIds.count > 0 {
                self.setSubscribed(subscribed: true)
            }
            if expiredProductIds.count > 0 {
                self.expiredIds.value = expiredProductIds
            }
            
            if activeProductIds.count == 0 && expiredProductIds.count > 0 {
                self.cancelledSubscription.value = true
            } else {
                self.cancelledSubscription.value = false
            }
            
            if iAPStatus.paidProdcutIds.count > 0 {
                self.hasPaid.value = true
            } else {
                self.hasPaid.value = false
            }
            
            if let farestDate = iAPStatus.willExpireProduct.max(by: { (front, after) -> Bool in
                return front.expireDate.timeIntervalSince1970 < after.expireDate.timeIntervalSince1970
            })?.expireDate {
                UserDefaults.standard.set(farestDate.timeIntervalSince1970, forKey: kFarestExpireDateKey)
            } else {
                UserDefaults.standard.removeObject(forKey: kFarestExpireDateKey)
            }
            
            return iAPStatus
        }
    }
}

class iAPItem : CustomDebugStringConvertible {
    var productId: IAPProductID
    var product: SKProduct? = nil
    
    init(productId: IAPProductID, product: SKProduct?) {
        self.productId = productId
        self.product = product
    }
    var debugDescription: String {
        return "id: \(productId.rawValue) \(product?.localizedPrice)"
    }
}
