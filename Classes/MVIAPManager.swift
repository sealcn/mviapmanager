//
//  MVIAPManager.swift
//  MVIAPManager
//
//  Created by rentonlin on 1/17/19.
//  Copyright © 2019 rentonlin. All rights reserved.
//

import Foundation
import SwiftyStoreKit
import RxSwift
import StoreKit

public typealias IAPManagerOnCompletePurchaseHandler = (_ hasPurchasedOrRestoredTranscation: Bool) -> Void
public typealias IAPManagerDidFinishPurchaseBlock = (_ purchase: PurchaseDetails) -> Void
public typealias IAPManagerDidRenewBlock = (_ purchase: Purchase) -> Void

public class MVIAPManager {
    static public let shared = MVIAPManager()
    private let disposeBag = DisposeBag()
    init() {
    }
    
    public var observePurchaseIAPFromAppStoreClosure: ShouldAddStorePaymentHandler? = nil
    public func observePurchaseIAPFromAppStore() {
        SwiftyStoreKit.shouldAddStorePaymentHandler = { [weak self] payment, product in
            return self?.observePurchaseIAPFromAppStoreClosure?(payment, product) ?? false
        }
    }
    
    public var didFinishPurchaseBlock: IAPManagerDidFinishPurchaseBlock? = nil
    public var didRenewBlock: IAPManagerDidRenewBlock? = nil
    
    public func completePurchase(completeHandelr: @escaping IAPManagerOnCompletePurchaseHandler) {
        SwiftyStoreKit.completeTransactions(atomically: true) { [weak self] (purchases) in
            var hasPurchaseOrRestored = false
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased:
                    self?.didRenewBlock?(purchase)
                    if purchase.needsFinishTransaction {
                        hasPurchaseOrRestored = true
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                case .restored:
                    if purchase.needsFinishTransaction {
                        hasPurchaseOrRestored = true
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
            completeHandelr(hasPurchaseOrRestored)
        }
    }
    
    public func verifyAndGetUnexpiredProductIds(mappedDictionary: [String: IAPRxWrapper.IAPType], secrect: String) -> Observable<[String]> {
        return IAPRxWrapper().verifyReceiptAndGetIAPStatus(
            with: mappedDictionary,
            sharedSecrect: secrect)
            .map{$0.activeProductIds}
    }

    public func verifyAndGetUnexpiredPurchaseResult(mappedDictionary: [String: IAPRxWrapper.IAPType], secrect: String) -> Observable<[ReceiptItem]> {
        return IAPRxWrapper().verifyReceiptAndGetReceiptResult(with: mappedDictionary, sharedSecrect: secrect)
    }
    
    public func purchase(iAPId: String) -> Observable<String> {
        return IAPRxWrapper().purchaseSKProduct(withId: iAPId).map({ [weak self] (purchase) -> String in
            self?.didFinishPurchaseBlock?(purchase)
            return purchase.productId
        })
    }
    
    public func restoreWithIAPs(supportedIAPAndType: [String: IAPRxWrapper.IAPType], secret: String) -> Observable<UserIAPStatus> {
        return IAPRxWrapper().restoreAndGetIAPStatus(
            with: supportedIAPAndType,
            sharedSecrect: secret)
    }
    
    public func getSKProducts(with productIds:Set<String>) -> Observable<[SKProduct]> {
        return IAPRxWrapper().getSKProducts(with: productIds)
    }
}
