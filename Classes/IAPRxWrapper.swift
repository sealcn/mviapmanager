//
//  IAPRxWrapper.swift
//  MVIAPManager
//
//  Created by rentonlin on 1/17/19.
//  Copyright © 2019 rentonlin. All rights reserved.
//

import Foundation

import RxSwift
import StoreKit
import SwiftyStoreKit

public struct PurchasedIAP {
    public let productId: String
    public let expireDate: Date
}

public struct UserIAPStatus {
    public let activeProductIds: [String]
    public let expiredProductIds: [String]
    public let paidProdcutIds: [String]
    public let willExpireProduct: [PurchasedIAP]
}

public class IAPRxWrapper {
    public enum IAPType {
        case Consumable
        case Non_Consumable
        case Auto_Renewable_Subscription
        case Non_Renewable_Subscription
    }
    
    func getSKProducts(with productIds:Set<String>) -> Observable<[SKProduct]> {
        return Observable<[SKProduct]>.create({ (observer) -> Disposable in
            SwiftyStoreKit.retrieveProductsInfo(productIds, completion: { (results) in
                if let error = results.error {
                    observer.onError(error)
                    return
                }
                let products = Array(results.retrievedProducts)
                observer.onNext(products)
                observer.onCompleted()
            })
            return Disposables.create()
        })
    }
    
    func purchaseSKProduct(withId productId:String) -> Observable<PurchaseDetails> {
        return Observable<PurchaseDetails>.create({ (observer) -> Disposable in
            SwiftyStoreKit.purchaseProduct(productId, completion: { (purchaseResult) in
                switch purchaseResult {
                case .success(let purchase):
                    observer.onNext(purchase)
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            })
            return Disposables.create()
        })
    }
    
    func purchaseSKProduct(product:SKProduct) -> Observable<PurchaseDetails> {
        return Observable<PurchaseDetails>.create({ (observer) -> Disposable in
            SwiftyStoreKit.purchaseProduct(product, completion: { (purchaseResult) in
                switch purchaseResult {
                case .success(let purchase):
                    observer.onNext(purchase)
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            })
            return Disposables.create()
        })
    }
    
    func restorePurchase() -> Observable<[Purchase]> {
        return Observable<[Purchase]>.create({ (observer) -> Disposable in
            SwiftyStoreKit.restorePurchases(completion: { (results) in
                if let (error, _) = results.restoreFailedPurchases.first {
                    observer.onError(error)
                } else if results.restoredPurchases.count > 0 {
                    observer.onNext(results.restoredPurchases)
                    observer.onCompleted()
                } else {
                    observer.onNext([])
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        })
    }
    
    func verifyReceipt(service: AppleReceiptValidator.VerifyReceiptURLType, sharedSecret: String?) -> Observable<ReceiptInfo> {
        return Observable<ReceiptInfo>.create({ (observer) -> Disposable in
            let appleValidator = AppleReceiptValidator(service: service, sharedSecret: sharedSecret)
            SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: { (verifyResult) in
                switch verifyResult {
                case .success(let receipt):
                    observer.onNext(receipt)
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            })
            return Disposables.create()
        })
    }
    
    public func verifySubscription(receipt:ReceiptInfo, type:SubscriptionType, productId: String) ->
        (willExpiresDate:Date?, expiredDate: Date?, purchased: Bool, isTrial: Bool) {
            let result = SwiftyStoreKit.verifySubscription(ofType: type, productId: productId, inReceipt: receipt)
            switch result {
            case .purchased(let (expiresDate, items)):
                let isTrial = items.filter { (item) -> Bool in
                    return item.isTrialPeriod
                    }.count > 0
                return (expiresDate, nil, true, isTrial)
            case .expired(let (expiresDate, items)):
                let isTrial = items.filter { (item) -> Bool in
                    return item.isTrialPeriod
                    }.count == items.count
                return (nil, expiresDate, true, isTrial)
            case .notPurchased:
                return (nil, nil, false, false)
            }
    }
    
    /// verifyPurchase
    ///
    /// - Parameters:
    ///   - productId: productId to verify
    ///   - receipt: receipt
    /// - Returns: return productId if purchased, otherwise nil
    func verifyPurchase(productId: String, receipt:ReceiptInfo) -> String? {
        let result = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
        switch result {
        case .purchased(let item):
            return item.productId
        case .notPurchased:
            return nil
        }
    }

    /// verifyPurchase
    ///
    /// - Parameters:
    ///   - productId: productId to verify
    ///   - receipt: receipt
    /// - Returns: return productId if purchased, otherwise nil
    func verifyReceipt(productId: String, receipt:ReceiptInfo) -> ReceiptItem? {
//        let receipts = IAPRxWrapper.getInAppReceipts(receipt: receipt)
//        let filteredReceiptsInfo = IAPRxWrapper.filterReceiptsInfo(receipts: receipts, withProductIds: [productId])
//
//        let nonCancelledReceiptsInfo = filteredReceiptsInfo.filter { receipt in receipt["cancellation_date"] == nil }
//
//        #if swift(>=4.1)
//            let receiptItems = nonCancelledReceiptsInfo.compactMap { ReceiptItem(receiptInfo: $0) }
//        #else
//            let receiptItems = nonCancelledReceiptsInfo.flatMap { ReceiptItem(receiptInfo: $0) }
//        #endif
//
//        // Verify that at least one receipt has the right product id
//        if let firstItem = receiptItems.first {
//            return firstItem
//        } else {
//            return nil
//        }
        let result = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
        switch result {
        case .purchased(let item):
            return item
        case .notPurchased:
            return nil
        }
    }
    
    /// Restore and verifyreceipt and get unexpired product
    ///
    /// - Parameters:
    ///   - productInfo: supported iAPs
    ///   - sharedSecrect: sharedSecret
    /// - Returns: observable of unexpired product ids
    public func restoreAndGetIAPStatus(
        with productInfo: [String: IAPType],
        sharedSecrect: String) -> Observable<UserIAPStatus> {
        return restorePurchase().flatMap({ (_) in
            self.verifyReceiptAndGetIAPStatus(with: productInfo, sharedSecrect: sharedSecrect)
        })
    }
    
    
    /// Verifyreceipt and get unexpired product
    ///
    /// - Parameters:
    ///   - productInfo: supported iAPs
    ///   - sharedSecrect: sharedSecret
    /// - Returns: observable of unexpired product ids
    public func verifyReceiptAndGetIAPStatus(
        with productInfo: [String: IAPType],
        sharedSecrect: String) -> Observable<UserIAPStatus> {
        #if DEBUG
        let service = AppleReceiptValidator.VerifyReceiptURLType.sandbox
        #else
        let service = AppleReceiptValidator.VerifyReceiptURLType.production
        #endif
        return verifyReceipt(service: service, sharedSecret: sharedSecrect).map({ (receipt) in
            self.getUserIAPStatus(from: receipt, productInfo: productInfo)
        })
    }

    public func verifyReceiptAndGetReceiptResult(with productInfo: [String: IAPType], sharedSecrect: String) -> Observable<[ReceiptItem]> {

        return getReceipt(with: sharedSecrect).map { (receip) in
            print(receip)
            var receiptItems: [ReceiptItem] = []
            for (productId, _) in productInfo {
                if let item = self.verifyReceipt(productId: productId, receipt: receip) {
                    receiptItems.append(item)
                }
            }
            return receiptItems
        }
    }

    private func getReceipt(with sharedSecrect: String) -> Observable<ReceiptInfo> {
        #if DEBUG
        let service = AppleReceiptValidator.VerifyReceiptURLType.sandbox
        #else
        let service = AppleReceiptValidator.VerifyReceiptURLType.production
        #endif
        return verifyReceipt(service: service, sharedSecret: sharedSecrect)
    }
    
    private func getUserIAPStatus(from receipt: ReceiptInfo, productInfo: [String: IAPType]) -> UserIAPStatus {
        var notExpiredProductIds: [String] = []
        var expiredProductIds: [String] = []
        var paidProductId: [String] = []
        var willExpireProdut: [PurchasedIAP] = []
        for (productId, iapType) in productInfo {
            switch iapType {
            case .Consumable:
                print("!!! not supported")
            case .Non_Consumable:
                if let _ = verifyPurchase(productId: productId, receipt: receipt) {
                    notExpiredProductIds.append(productId)
                    willExpireProdut.append(PurchasedIAP(productId: productId, expireDate: Date.distantFuture))
                }
            case .Auto_Renewable_Subscription:
                let info = verifySubscription(receipt: receipt, type: .autoRenewable, productId: productId)
                if info.purchased {
                    if let willExpiresDate = info.willExpiresDate {
                        notExpiredProductIds.append(productId)
                        willExpireProdut.append(PurchasedIAP(productId: productId, expireDate: willExpiresDate))
                    } else if let _ = info.expiredDate {
                        expiredProductIds.append(productId)
                    }
                    if !(info.isTrial) {
                        paidProductId.append(productId)
                    }
                }
            case .Non_Renewable_Subscription:
                print("!!! not supported")
            }
        }
        return UserIAPStatus(activeProductIds: notExpiredProductIds,
                             expiredProductIds: expiredProductIds,
                             paidProdcutIds: paidProductId,
                             willExpireProduct: willExpireProdut)
    }
}

extension IAPRxWrapper {
    private class func getInAppReceipts(receipt: ReceiptInfo) -> [ReceiptInfo]? {

        let appReceipt = receipt["receipt"] as? ReceiptInfo
        return appReceipt?["in_app"] as? [ReceiptInfo]
    }

    private class func filterReceiptsInfo(receipts: [ReceiptInfo]?, withProductIds productIds: Set<String>) -> [ReceiptInfo] {

        guard let receipts = receipts else {
            return []
        }

        // Filter receipts with matching product ids
        let receiptsMatchingProductIds = receipts
            .filter { (receipt) -> Bool in
                if let productId = receipt["product_id"] as? String {
                    return productIds.contains(productId)
                }
                return false
            }

        return receiptsMatchingProductIds
    }
}
